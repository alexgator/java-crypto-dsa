package dsa.gen;

import dsa.dto.DSAKeyPair;
import dsa.dto.DSAParams;
import dsa.io.IOUtil;
import sun.security.jca.JCAUtil;

import java.math.BigInteger;
import java.security.SecureRandom;

public class DSAKeyGen {
    private static SecureRandom secureRandom = JCAUtil.getSecureRandom();

    public static DSAKeyPair generateKeys(DSAParams params) {
        IOUtil.logTest("Generating keys...");
        int qLength = params.getQ().bitLength();
        BigInteger x = new BigInteger(qLength / 2, secureRandom);
        if (x.compareTo(params.getQ()) > -1) {
            throw new RuntimeException("x is greater than q");
        }
        BigInteger y = params.getG().modPow(x, params.getP());


        IOUtil.logTest("Keys generated successfully");
        IOUtil.logTest("y = " + y);
        IOUtil.logTest("x = " + x);
        return new DSAKeyPair(x, y);
    }
}
