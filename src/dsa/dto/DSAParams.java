package dsa.dto;

import java.math.BigInteger;
import java.security.MessageDigest;

public class DSAParams {
    private BigInteger p;
    private BigInteger q;
    private BigInteger g;
    private BigInteger seed;
    private MessageDigest hash;
    public DSAParams(BigInteger p, BigInteger q, BigInteger g, BigInteger seed, MessageDigest hash) {
        this.p = p;
        this.q = q;
        this.g = g;
        this.seed = seed;
        this.hash = hash;
    }

    public byte[] hash(byte[] input) {
        return hash.digest(input);
    }

    public BigInteger getP() {
        return p;
    }

    public BigInteger getQ() {
        return q;
    }

    public BigInteger getG() {
        return g;
    }

    public BigInteger getSeed() {
        return seed;
    }
}
