package dsa.dto;

import java.math.BigInteger;

public class DSAKeyPair {
    private BigInteger x;
    private BigInteger y;
    public DSAKeyPair(BigInteger x, BigInteger y) {
        this.x = x;
        this.y = y;
    }

    public BigInteger getX() {
        return x;
    }

    public BigInteger getY() {
        return y;
    }
}
