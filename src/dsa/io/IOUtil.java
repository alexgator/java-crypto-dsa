package dsa.io;

public class IOUtil {

    private static boolean isTest = false;
    private static boolean isVerbose = false;

    public static void setTest(boolean test) {
        IOUtil.isTest = test;
    }

    public static void setVerbose(boolean verbose) {
        IOUtil.isVerbose = verbose;
    }

    public static void logTestVerbose(Object o) {
        if (isVerbose) logTest(o);
    }

    public static void logTest(Object o) {
        if (isTest) println(o);
    }

    public static void println(Object o) {
        System.out.println(o);
    }
}
