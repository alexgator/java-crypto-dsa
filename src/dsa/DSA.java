package dsa;

import dsa.dto.DSAKeyPair;
import dsa.dto.DSAParams;
import dsa.io.IOUtil;
import sun.security.jca.JCAUtil;

import java.math.BigInteger;
import java.security.SecureRandom;

public class DSA {

    // Secure random from Java Cryptography Architecture
    private static SecureRandom secureRandom = JCAUtil.getSecureRandom();

    private DSAParams params;

    public DSA(DSAParams params) {
        this.params = params;
    }

    public DSA.Sign sign(String message, DSAKeyPair key) {
        IOUtil.logTest("Signing message...");
        BigInteger r;
        BigInteger k;
        BigInteger s;
        BigInteger hash;
        BigInteger kInv;
        do {
            do {
                k = new BigInteger(params.getQ().bitLength() / 2, secureRandom);
                r = (params.getG().modPow(k, params.getP()))
                        .mod(params.getQ());

            } while (r.equals(BigInteger.ZERO));

            kInv = modInverse(k, params.getQ());

            hash = new BigInteger(1, params.hash(message.getBytes()));

            s = hash
                    .add(key.getX().multiply(r))
                    .multiply(kInv)
                    .mod(params.getQ());
        } while (s.equals(BigInteger.ZERO));
        IOUtil.logTest("Message signed successfully.");
        IOUtil.logTest("s = " + s);
        IOUtil.logTest("r = " + r);
        return new Sign(r, s);
    }

    public boolean check(String message, DSA.Sign sign, BigInteger pubKey) {
        IOUtil.logTest("Checking message sign...");
        BigInteger y = pubKey;
        BigInteger w = modInverse(sign.getS(), params.getQ());

        BigInteger hash = new BigInteger(1, params.hash(message.getBytes()));

        BigInteger u1 = hash
                .multiply(w)
                .mod(params.getQ());
        IOUtil.logTest("u1 = " + u1);

        BigInteger u2 = sign.getR()
                .multiply(w)
                .mod(params.getQ());
        IOUtil.logTest("u2 = " + u2);


        BigInteger v = params.getG().modPow(u1, params.getP())
                .multiply(y.modPow(u2, params.getP()))
                .mod(params.getQ());
        IOUtil.logTest("v = " + v);
        return v.equals(sign.getR());
    }

    public static class Sign {
        private BigInteger r;
        private BigInteger s;

        public Sign(BigInteger r, BigInteger s) {
            this.r = r;
            this.s = s;
        }

        public BigInteger getR() {
            return r;
        }

        public BigInteger getS() {
            return s;
        }
    }

    private static class Euclid {

        BigInteger d;
        BigInteger x;
        BigInteger y;

        Euclid(BigInteger d, BigInteger x,BigInteger y) {
            this.d = d;
            this.x = x;
            this.y = y;
        }
    }

    private static Euclid getEuclid(BigInteger a, BigInteger b) {
        Euclid tmp = new Euclid(a, BigInteger.ONE, BigInteger.ZERO);
        Euclid tmp2;

        if (b.equals(BigInteger.ZERO)) {
            return tmp;
        }

        tmp2 = getEuclid(b, a.mod(b));

        tmp.d=  tmp2.d;
        tmp.x = tmp2.y;
        tmp.y = tmp2.x.subtract(a.divide(b).multiply(tmp2.y));

        return tmp;
    }

    private static BigInteger modInverse(BigInteger value, BigInteger m) {
        return getEuclid(value, m).x;
    }
}
