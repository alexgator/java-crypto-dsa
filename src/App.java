import dsa.DSA;
import dsa.dto.DSAKeyPair;
import dsa.dto.DSAParams;
import dsa.gen.DSAKeyGen;
import dsa.gen.DSAParamsGen;
import dsa.io.IOUtil;

import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
    public static void main(String[] args) {
        try {
            IOUtil.setTest(true);

            DSAParams params = new DSAParamsGen.Builder()
                    .withSHA1()
                    .build();
            DSA dsa = new DSA(params);

            String message = "Hello world!";

            DSAKeyPair key = DSAKeyGen.generateKeys(params);

            DSA.Sign sign = dsa.sign(message, key);

            boolean valid = dsa.check(message, sign, key.getY());

            System.out.println("Valid: " + valid);

        } catch (Throwable e) {
            Logger.getLogger("DSA").log(Level.WARNING, "Exception:", e);
        }
    }
}
